//
//  NewsTableViewCell.swift
//  NewsApp
//
//  Created by Rajesh Mani on 07/09/22.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newImageView: UIImageView!
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // Mark: loading categories in collectionviewcell
    func configureCell(articleVM: ArticleViewModel){
        self.newsTitleLabel.text = articleVM.title
        self.newsTitleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.newsTitleLabel.textColor = .black
        self.sourceLabel.text = articleVM.source
        self.authorLabel.text = articleVM.author
        self.sourceLabel.font = UIFont.boldSystemFont(ofSize: 8.0)
        self.authorLabel.font = UIFont.boldSystemFont(ofSize: 8.0)
        self.backgroundColor = .clear
        self.newsDescriptionLabel.textColor = .darkGray
        self.newsDescriptionLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
        self.newsDescriptionLabel.text = articleVM.description
        self.newImageView.layer.cornerRadius = 15
        if let urlString = articleVM.imageURL {
            self.newImageView.loadImageFromUrl(urlString: urlString)
        }else {
            self.newImageView.placeHoldImage
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}



