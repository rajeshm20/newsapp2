//
//  NewCategoryCell.swift
//  NewsApp
//
//  Created by Rajesh Mani on 08/09/22.
//

import UIKit

class NewCategoryCell: UICollectionViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    let cellIdentifier = "NewCategoryCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.layer.addBorder(edge: .bottom, color: .white, thickness: 2.0)
    }
    
    func configCell(title: String){
        self.contentView.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1).withAlphaComponent(1)
        self.contentView.layer.cornerRadius = 6
        self.titleLabel.text = title
        self.titleLabel.textColor = .white
        self.titleLabel.font = UIFont(name: "ArialBold", size: 15)
//        cell.titleLabel.layer.addBorder(edge: .bottom, color: .systemRed, thickness: 5.0)
    }
}
