//
//  Extension.swift
//  NewsApp
//
//  Created by Rajesh Mani on 07/09/22.
//

import Foundation
import UIKit
import Alamofire

extension UIView{

    func activityStartAnimating(activityColor: UIColor, backgroundColor: UIColor) {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        
        backgroundView.addSubview(activityIndicator)

        self.addSubview(backgroundView)
    }

    func activityStopAnimating() {
        DispatchQueue.main.async {
            if let background = self.viewWithTag(475647){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
        }
    }
}

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    //Mark: Custom function to fetch and load image from cache in tableviewcell
    func loadImageFromUrl(urlString: String, mode: UIView.ContentMode = .scaleAspectFill)  {
        imageCache.removeAllObjects()
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            self.image = imageFromCache
            return
        }
        self.contentMode = mode
        DispatchQueue.main.async {
            self.activityStartAnimating(activityColor: .black, backgroundColor: .clear)
        }
        AF.request(urlString, method: .get).response { (responseData) in
            DispatchQueue.main.async {
                self.activityStopAnimating()
            }
            if let data = responseData.data {
                DispatchQueue.main.async {
                    if let imageToCache = UIImage(data: data){
                        imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                        self.image = imageToCache
                    }else {
                        self.image = UIImage(named: "placeholderImage")!
                    }
                }
            }
        }
        
    }
    
    //Mark: Placing a placeholder image
    var placeHoldImage: UIImage {
        
        return UIImage(named: "placeholderImage")!
        
    }
}


extension CALayer {
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        let border = CALayer()
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 1)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - 1, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: 1, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - 1, y: 0, width: 1, height: self.frame.height)
            break
        default:
            break
        }
        border.backgroundColor = color.cgColor;
        self.addSublayer(border)
    }
}
