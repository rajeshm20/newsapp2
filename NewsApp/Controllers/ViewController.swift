//
//  ViewController.swift
//  NewsApp
//
//  Created by Rajesh Mani on 07/09/22.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet var newsTableView: UITableView!
    @IBOutlet var categoryCollectionView: UICollectionView!
    var alertTimer: Timer?
    var networkTimer: Timer?
    var alermsg: String = ""
    let networkMgr = NetworkManager.sharedInstance
    let connection = ConnectionManager.sharedInstance
    private var articleListVM: ArticleListViewModel!
    var selectedNewsURLString:String?
    let monitor = NetworkMonitor.shared
    let categories = API.Category.allCases
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkNetwork()
        self.view.backgroundColor = #colorLiteral(red: 0.897295177, green: 0.8489845991, blue: 0.833090961, alpha: 1).withAlphaComponent(1)
        newsTableView.delegate = self
        newsTableView.dataSource = self
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        newsTableView.backgroundColor = .clear
        newsTableView.rowHeight = UITableView.automaticDimension
        newsTableView.estimatedRowHeight = 250
        categoryCollectionView.collectionViewLayout.invalidateLayout()
        categoryCollectionView.backgroundColor = .clear
        newsTableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
        categoryCollectionView.register(UINib(nibName: "NewCategoryCell", bundle: nil), forCellWithReuseIdentifier: NewCategoryCell().cellIdentifier)
        DispatchQueue.main.async {
            self.view.activityStartAnimating(activityColor: UIColor.black, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        }
        self.fetchArticles()
    }
    // Mark: Fetch Today top headlines by category eg: business entertainment general health science sports technology
    func getArticlesByCategory(cateogry: API.Category.RawValue, country: API.Country.RawValue) {
       
        DispatchQueue.main.async {
            if self.articleListVM != nil {
                debugPrint("articlelistempty")
                self.articleListVM = nil
            self.newsTableView.reloadData()
            }
        }
        
        let url = API().getServiceURL(country: country, category: cateogry)
        let reachable = connection.reachability.isReachable
        if reachable {
            self.alertTimer?.invalidate()
            self.alertTimer = nil
        self.networkMgr.getTopHeadlinesForCategory(url: url, completionHandler: { result in
            
            // Mark: function to call today's top headlines from News API
//                guard self.alertTimer == nil else { return }
//                self.alertTimer?.invalidate()
                    DispatchQueue.main.async {
                        self.view.activityStopAnimating()
                    }
                    switch result{
                    case .success(let articles):
//                        debugPrint("articles---\(articles ?? [])")
                        self.articleListVM = ArticleListViewModel(articles: articles)
                        debugPrint("articleListVMCount---\(self.articleListVM.articles.count)")
                        if self.articleListVM != nil {
                            DispatchQueue.main.async {
                                self.newsTableView.reloadData()
                            }
                        }
                    case .failure(let error):
                        debugPrint("error----\(error)")
                        DispatchQueue.main.async {
                            if error.localizedDescription == "internetDown" {
                                self.alermsg = "Check your internet connection!"
                                self.showAlert()
                            }
                        }
                    }
            
        })
        } else {

            DispatchQueue.main.async {
                self.checkNetwork()
                self.alermsg = "Check your internet connection!"
                self.view.activityStopAnimating()
                    guard self.alertTimer == nil else { return }
                    self.alertTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.showAlert), userInfo: nil, repeats: true)
                    self.alertTimer!.fire()
            }
        }
    }
    //Mark: Try to check network connection and initiate fetch articles
    @objc func checkNetwork(){
        DispatchQueue.main.async {
            
        let reachable = self.connection.reachability.isReachable
            if !self.monitor.isReachable {
            debugPrint("checkNetworkIF")
//            self.alertTimer?.invalidate()
//            self.alertTimer = nil
//            self.networkTimer?.invalidate()
//            self.networkTimer = nil
            self.fetchArticles()
        }else {
            debugPrint("checkNetworkELSE")
//            guard self.networkTimer == nil else { return }
//            self.networkTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.checkNetwork), userInfo: nil, repeats: true)
//            self.networkTimer!.fire()
            self.checkNetwork()
        }
        }
    }
    // Mark: Fetch articles top headlines
    func fetchArticles(){
        let reachable = connection.reachability.isReachable
        // Mark: function to call today's top headlines from News API
        
        if reachable {
            self.networkMgr.getTopHeadlines(completionHandler: { result in
                
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                }
                switch result{
                case .success(let articles):
                    debugPrint("articles---\(articles ?? [])")
                    self.articleListVM = ArticleListViewModel(articles: articles ?? [])
                    if self.articleListVM != nil {
                        DispatchQueue.main.async {
                            self.newsTableView.reloadData()
                        }
                    }
                case .failure(let error):
                    debugPrint("error----\(error)")
                }
            })
        } else {
            DispatchQueue.main.async {
                self.checkNetwork()
                self.alermsg = "Check your internet connection!"
                self.view.activityStopAnimating()
                self.showAlert()
            }
            
        }
    }
    
    @objc func showAlert(){
        self.checkNetwork()
        self.showAlertButtonTapped(error: alermsg)
    }
    func showAlertButtonTapped(error: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
}



extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.articleListVM == nil ? 0 : self.articleListVM.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articleListVM.numberOfRowsInSection(self.articleListVM.numberOfRowsInSection(section))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as! NewsTableViewCell
        let articleVM = self.articleListVM.articleAtIndex(indexPath.row)
        cell.configureCell(articleVM: articleVM)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let articleVM = self.articleListVM.articleAtIndex(indexPath.row)
        if articleVM.storyURL != "" {
            self.selectedNewsURLString = articleVM.storyURL
            self.performSegue(withIdentifier: "toStory", sender: nil)
        } else {
            debugPrint("url not available")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toStory" {
            if let nextViewController = segue.destination as? WebViewController {
                nextViewController.urlString = self.selectedNewsURLString! //Or pass any values
            }
        }
    }
    
}


// Mark: Top News categories collectionview
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return API.Category.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NewCategoryCell().cellIdentifier, for: indexPath) as! NewCategoryCell
        let cat = categories[indexPath.row].rawValue.uppercased()
        cell.configCell(title: cat)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cat = categories[indexPath.row].rawValue
        
        self.getArticlesByCategory(cateogry: cat, country: API.Country.us.rawValue)
    }
}
