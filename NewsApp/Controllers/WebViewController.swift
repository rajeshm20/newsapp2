//
//  WebViewController.swift
//  NewsApp
//
//  Created by Rajesh Mani on 08/09/22.
//

import UIKit
import WebKit
import Network


class WebViewController: UIViewController {
    let connManager = ConnectionManager.sharedInstance
    let monitor = NWPathMonitor()
    @IBOutlet weak var newStoryWebVw: WKWebView!
    var urlString: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
     
        monitor.pathUpdateHandler = { path in
           if path.status == .satisfied {
              print("Connected")
           } else {
              print("Disconnected")
               DispatchQueue.main.async {
                   AlertView.showAlert(view: self, message: "Check your Internet connection")
               }
           }
           print(path.isExpensive)
        }
        self.view.activityStartAnimating(activityColor: .black, backgroundColor: .clear)
        if urlString != "" {
            self.newStoryWebVw.load(URLRequest(url: URL(string: urlString)!))
            self.view.activityStopAnimating()
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func showAlertButtonTapped(error: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertViewController")
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
}
