//
//  AlertViewController.swift
//  NewsApp
//
//  Created by Rajesh Mani on 08/09/22.
//

import UIKit

class AlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var alertLabel: UILabel!
    @IBOutlet var alertICon: UIImageView!
    let monitor = NetworkMonitor.shared

    var alertMsg = "Check your internet connection!"
    override func viewDidLoad() {
        super.viewDidLoad()
        monitor.startMonitoring()
//        let blurEffect = UIBlurEffect(style: .dark)
//        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
//        blurredEffectView.frame = view.bounds
//        view.addSubview(blurredEffectView)
        alertLabel.text = alertMsg
        alertICon.image = UIImage(systemName: "wifi.exclamationmark")
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = .clear
//        blurredEffectView.addSubview(alertView)
        // Do any additional setup after loading the view.
        
        if monitor.isReachable {
            perform(#selector(removeAlert), with: nil, afterDelay: 0.5)
        }else {
            perform(#selector(removeAlert), with: nil, afterDelay: 0.8)
        }
    }
    
    @objc func removeAlert(){
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class AlertView: NSObject {

    class func showAlert(view: UIViewController , message: String) {
        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        DispatchQueue.main.async {
            view.present(alert, animated: true, completion: nil)
        }
       
    }
}
