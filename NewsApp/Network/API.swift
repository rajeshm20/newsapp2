//
//  API.swift
//  NewsApp
//
//  Created by Rajesh Mani on 09/09/2022.
//

import Foundation


class API {
    
    static var baseURL: String {
        return "https://newsapi.org/v2/"
    }
    static var topHeadlines: String {
        return "top-headlines"
    }
    static var everything: String {
        return "everything"
    }
    static var topHeadLinesSources: String {
        return "top-headlines/sources"
    }
    static var apiKey: String {
        return "8f43c37692294b1b92c6b57583d539f2"
    }

    enum Category: String, CaseIterable {
        case business = "business"
        case entertainment = "entertainment"
        case general = "general"
        case health = "health"
        case science = "science"
        case sports = "sports"
        case technology = "technology"
    }
    
    enum Country: String {
//        lvmamxmyngnlnonzphplptrorsrusasesgsiskthtrtw
        case ae = "ae"
        case ar = "ar"
        case at = "at"
        case au = "au"
        case be = "be"
        case bg = "bg"
        case br = "br"
        case ca = "ca"
        case ch = "ch"
        case cn = "cn"
        case co = "co"
        case cu = "cu"
        case cz = "cz"
        case de = "de"
        case eg = "eg"
        case fr = "fr"
        case gb = "gb"
        case gr = "gr"
        case hk = "hk"
        case hu = "hu"
        case id = "id"
        case ie = "ie"
        case il = "il"
        case ind = "in"
        case it = "it"
        case jp = "jp"
        case kr = "kr"
        case lt = "lt"
        case us = "us"
        case za = "za"
        case ve = "ve"
        case ua = "ua"
    }
    
    static var topHeadlinesApiURL: URL  {
        return URL(string: "\(baseURL)\(topHeadlines)?country=\(Country.au)&category=\(Category.general)&apiKey=\(apiKey)")!
    }
    
    func getServiceURL(country: Country.RawValue, category: Category.RawValue) -> URL {
        return URL(string: "\(API.baseURL)\(API.topHeadlines)?country=\(country)&category=\(category)&apiKey=\(API.apiKey)")!
    }
}
