//
//  NetworkManager.swift
//  NewsApp
//
//  Created by Rajesh Mani on 07/09/22.
//

import Foundation
import Alamofire


class NetworkManager: NSObject {
    
    static let sharedInstance = NetworkManager()
    let networkConn = ConnectionManager.sharedInstance

    
    private override init() { }

    enum NetworkError: Error {
        case decodingError
        case domainError
        case urlError
        case internetDown
    }
        
        func getArticles(url: URL?, completionHandler:@escaping (Result<[Article], NetworkError>) -> Void){
            guard let url = url else {
                return completionHandler(.failure(.domainError))
            }
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                guard let data = data, error == nil else {
                    completionHandler(.failure(.domainError))
                    return
                }
                let result = try? JSONDecoder().decode(ArticleList.self, from: data)
                
                if let result = result {
                    DispatchQueue.main.async {
                        completionHandler(.success(result.articles ?? [] ))
                    }
                } else {
                    completionHandler(.failure(.decodingError))
                }
                
            }.resume()
            
    }
    func getTopHeadlinesForCategory(url: URL, completionHandler:@escaping (Result<[Article], NetworkError>) -> Void){
        
        let userinitiated = DispatchQueue.global(qos: .userInitiated)
//        networkConn.reachability.whenUnreachable = { status in
//            print("connection--\(status)")
//            
//            return
//        }
        let reachable = networkConn.reachability.isReachable
        if reachable {

        AF.request(url)
            .responseString { response in
//                debugPrint("Response String: \(response.value)")
            }
            .responseDecodable(of: ArticleList.self, queue: userinitiated) { response in
                debugPrint("Response: \(response)")

                debugPrint("Response DecodableType: \(String(describing: response.value))")
                if let val = response.value {
                    completionHandler(.success(val.articles ?? []))

                }else {
                    completionHandler(.failure(.domainError))
                }
            }
            
        } else {
            completionHandler(.failure(.internetDown))

        }
    }
    
    func getTopHeadlines(completionHandler:@escaping (Result<[Article], NetworkError>) -> Void){
        
        let userinitiated = DispatchQueue.global(qos: .userInitiated)
//        networkConn.reachability.whenUnreachable = { status in
//            print("connection--\(status)")
//
//            return
//        }
        
        AF.request(API.topHeadlinesApiURL)
            .responseString { response in
//                debugPrint("Response String: \(response.value)")
            }
            .responseDecodable(of: ArticleList.self, queue: userinitiated) { response in
                debugPrint("Response: \(response)")

                debugPrint("Response DecodableType: \(String(describing: response.value))")
                if let val = response.value {
                    completionHandler(.success(val.articles ?? []))

                }else {
                    completionHandler(.failure(.domainError))
                }
            }
    }
}
