//
//  ArticleViewModel.swift
//  GoodNewsMVVM
//
//  Created by Rajesh Mani on 07/09/22.
//

import Foundation

struct ArticleListViewModel{
    
    var articles: [Article]
}

extension ArticleListViewModel {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        
        return self.articles.count
    }
    
    func articleAtIndex(_ index: Int) -> ArticleViewModel {
        
        let article = self.articles[index]
        return ArticleViewModel(article)
    }
}

struct ArticleViewModel {
    
    private var article: Article?
}


extension ArticleViewModel {
    
    init(_ article: Article){
        
        self.article = article
    }
}


extension ArticleViewModel {
    
    var title: String {
        return article?.title ?? ""
    }
    
    var description: String {
        return article?.articleDescription ?? ""
    }
    
    var imageURL: String? {
        return article?.urlToImage
    }
    
    var source: String {
        return "  Source - " + (article?.source?.name ?? "")
    }
    
    var author: String {
        return "  Author - " + (article?.author ?? "")
    }
    
    var storyURL: String {
        return  (article?.url ?? "")
    }
}

